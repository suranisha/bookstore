<?php
require_once 'dompdf-master/autoload.inc.php';
require 'PHPMailer-master/PHPMailerAutoload.php';
use Dompdf\Dompdf;

$mail = new PHPMailer;
if(isset($_POST['email'])){
// instantiate and use the dompdf class
$dompdf = new Dompdf();
$dompdf->set_option('isHtml5ParserEnabled', true);
$content=' <!doctype html> 
        <html> 
        <head> 
            <link rel="stylesheet" href="old-files/main.css" type="text/css" /> 
        </head> 
        <body>
<div id="container">
    <header>
    	<img class="logo" src="old-files/images/mainMenu/logo.png" />
        <div class="blackBar">
        	
        </div>
         <div class="goldBar">
         	
         </div>
    </header>
    
    <div id="mainBody">
    	<!-----Main body starts here---->
        <article>
       	   <div id="invoicenumber">
		   <h3>Invoice</h3><h5>I-0001</h5><h6>Date: 09/09/1999</h6>
		   </div>
        
<div id="billingshipping">
			<div class="billingInfo">
				Billing Information
					<div class="billingDetails">
					Group 3<br/><br/>
					1234 Street Dr.<br/>
					Marietta, Georgia. 30127<br/><br/>
					Phone:678-602-0009<br/>
					xyz@gmail.xyz
					</div>
			</div>
			<div class="shippingInfo">
				Shipping Information
					<div class="billingDetails">
					Group 3<br/><br/>
					1234 Street Dr.<br/>
					Marietta, Georgia. 30127<br/><br/>
					Phone:678-602-0009<br/>
					xyz@gmail.xyz
					</div>
			</div>
			</div><!--BillingShipping-->
			<div id="itemInfo"><h2>Item Purchased</h2>
			</div>
			<div class="itemDetailsContainer" id="itemDetailLabel">
				<div class="itemDetails">
				<p class="number-itemInfo">ItemNumber</p>
				<p class="title-itemInfo">Description</p>
				<p class="qty-itemInfo">Quantity</p>
				<p class="price-itemInfo">Price</p>
		
			</div>
			</div>
			<div class="itemDetailsContainer">
				<div class="itemDetails">
				<p class="number-itemInfo">1</p>
				<p class="title-itemInfo">Essential of Economics<br/>by Karim<br/>ISBN:090078601</p>
				<p class="qty-itemInfo">1</p>
				<p class="price-itemInfo">$22<br/>New</p>
		
			</div>
			</div>
			<div class="itemDetailsContainer">
				<div class="itemDetails">
				<p class="number-itemInfo">1</p>
				<p class="title-itemInfo">Essential of Economics<br/>by Karim<br/>ISBN:090078601</p>
				<p class="qty-itemInfo">1</p>
				<p class="price-itemInfo">$22<br/>New</p>
		
			</div>
			</div>
			<div class="itemDetailsContainer">
				<div class="itemDetails">
				<p class="number-itemInfo">1</p>
				<p class="title-itemInfo">Essential of Economics<br/>by Karim<br/>ISBN:090078601</p>
				<p class="qty-itemInfo">1</p>
				<p class="price-itemInfo">$22<br/>New</p>
		
			</div>
			</div>
			<div class="itemDetailsContainer">
				<div class="itemDetails">
				<p class="number-itemInfo">1</p>
				<p class="title-itemInfo">Essential of Economics<br/>by Karim<br/>ISBN:090078601</p>
				<p class="qty-itemInfo">1</p>
				<p class="price-itemInfo">$22<br/>New</p>
		
			</div>
			</div>
			<div class="itemDetailsContainer">
				<div class="itemDetails">
				<p class="number-itemInfo">1</p>
				<p class="title-itemInfo">Essential of Economics<br/>by Karim<br/>ISBN:090078601</p>
				<p class="qty-itemInfo">1</p>
				<p class="price-itemInfo">$22<br/>New</p>
		
			</div>
			</div>
			<div class="itemDetailsContainer">
				<div class="itemDetails">
				<p class="number-itemInfo">1</p>
				<p class="title-itemInfo">Essential of Economics<br/>by Karim<br/>ISBN:090078601</p>
				<p class="qty-itemInfo">1</p>
				<p class="price-itemInfo">$22<br/>New</p>
		
			</div>
			</div>
			<div class="itemDetailsContainer">
				<div class="itemDetails">
				<p class="number-itemInfo">1</p>
				<p class="title-itemInfo">Essential of Economics Essential of Economics Essential of Economics Essential of Economics<br/>by Karim<br/>ISBN:090078601</p>
				<p class="qty-itemInfo">1</p>
				<p class="price-itemInfo">$22<br/>New</p>
		
			</div>
			</div>
			<div class="itemDetailsContainer">
				<div class="itemDetails">
				<p class="number-itemInfo">1</p>
				<p class="title-itemInfo">Essential of Economics<br/>by Karim<br/>ISBN:090078601</p>
				<p class="qty-itemInfo">1</p>
				<p class="price-itemInfo">$22<br/>New</p>
		
			</div>
			</div>
			<div class="itemDetailsContainer">
				<div class="itemDetails">
				<p class="number-itemInfo">1</p>
				<p class="title-itemInfo">Essential of Economics<br/>by Karim<br/>ISBN:090078601</p>
				<p class="qty-itemInfo">1</p>
				<p class="price-itemInfo">$22<br/>New</p>
		
			</div>
			</div>			
			<div id="finalPrice">
		<p id="subTotal">Subtotal</p><p class="Fvalue">$22.9</p><br/>
		<p id="shipping">Shipping</p><p class="Fvalue">$22</p><br/>
		<p id="tax">Tax</p><p class="Fvalue">$2</p><br/>
		<p id="total">Total</p><p id="totalvalue">$99999</p>
		
		</div>
		
		
		
		</article>
		
		
        
        
        
        
        
        
    </div><!-----Main body ends here---->
  </div><!--container end-->
  
  <!----JS starts here----->
  
		</body> 
        </html>';
$dompdf->loadHtml($content);

// Render the HTML as PDF
$dompdf->render();
$dir="invoices/".date("Ymd");
//check if directory exist? if not, create a directory
if(!(is_dir($dir)) && !file_exists($dir)){
	mkdir($dir);
}
$file=$dir."/invoice".date("His").".pdf";
// Output the generated PDF to Browser
file_put_contents($file, $dompdf->output()); 
$mail->setFrom('test@valuehosted.com', 'Worth of Coding');
$email=$_POST['email'];
        $mail->addAddress($email, 'Invoice');
        $mail->Subject = 'PHPMailer file sender';
        $mail->msgHTML("My message body");
		  $mail->addAttachment($file, 'Invoice.pdf');
		  $msg="";
 if (!$mail->send()) {
            $msg .= "Mailer Error: " . $mail->ErrorInfo;
        } else {
            $msg .= "Message sent!";
        }
		echo $msg;
    

//$dompdf->stream();
}


?>
<form method="POST" target="">
<input type="email" placeholder="Input Email" name="email" required/>
<input type="submit" value="submit" name="submit"/>

</form>