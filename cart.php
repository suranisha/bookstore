<?php


require("_includes/header.php"); 
//update qty
if(isset($_GET["update"])){
	$_SESSION["totalPrice"] = 0.00;
	$_SESSION["totalItemsInCart"] = 0;
	
	foreach($_GET as $key=>$val){
		$book = explode("_", $key);
		if(sizeof($book)<2) continue;
		$priceobj = new Price();
		
		$prices = $priceobj->find_by_id_type($book[0],$book[1]);
		$qty = $prices->quantity;
		foreach($_SESSION['cart'] as $item){
			if($item->id==$book[0] && $item->type==$book[1]){
				if($qty>=$val){
					$item->quantity=intval($val);
					if($val>=0){
						$_SESSION["totalItemsInCart"] += $item->quantity;
						$_SESSION["totalPrice"] += $item->quantity*$item->price;
					}
				}else {
					$item->quantity=$qty;
					if($val>=0){
						$_SESSION["totalItemsInCart"] += $qty;
						$_SESSION["totalPrice"] += $qty*$item->price;
					}
				}
			}
		}
	}
	foreach($_SESSION["cart"] as $key => $eachBook){
			if($eachBook->quantity <= 0){
				unset($_SESSION["cart"][$key]);	
			}
		}
	return;
}
?>

    <div id="mainBody">
    	<!-----Main body starts here---->
        
        <h2>My Cart</h2>
        <p>&nbsp;</p>
        <table id="tableCart">
            <? 
			if(isset($_SESSION["cart"])){ 
			foreach($_SESSION["cart"] as $eachCartItem){ $book = new Book(); $book = $book->find_by_id($eachCartItem->id);?>
            <tr>
              <td width="219"><img src="images/bookCover/<? echo $book->isbn ?>.jpg" width="150" height="200" alt=""/><br>
			  </td>
              <td width="372"><h2><? echo $book->title ?></h2>
                <p>By: <? echo $book->author ?></p>
                <p>ISBN: <? echo $book->isbn ?></p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p><br>
                </p>
               	<input name="Submit" type="submit" id="goldButton" value="View Details" onclick="window.location='bookdetails.php?id=<? echo $book->id ?>';" />
                
              </td>
              <td width="196"><p>Qty:<input type="number" value="<? echo $eachCartItem->quantity ?>" id="updateQuantity" name="qty" min=0 required/></p><br />
              <input id="redButton" type="submit" value="Remove" data-id="<? echo $book->id; ?>" class="redButton" data-type="<? echo $eachCartItem->type; ?>" />
              
              </td>
              <td width="426"><p>$<? echo $eachCartItem->price ?>
              </p>
              <p><? echo $eachCartItem->typeText ?></p></td>
            </tr>
            <? }} ?>
            <tr>
              <td width="219"></td>
              <td width="372"></td>
              <td width="196"><p style="margin-left:30px;">Total: <? echo $_SESSION["totalItemsInCart"] ?></p>
              <br />
              <input style="border-radius:10px; margin:0; background-color: #0C0;" id="updateQuantityButton" class="button-cart"  name="update" type="submit" value="Update" <?php /*?>onclick="window.location='cart.php'"<?php */?>>
              </td>
              <td width="426"><p>$<? echo $_SESSION["totalPrice"] ?></p></td>
            </tr>
            <tr>
              <td width="219"><input id="redButton" type="submit" value="Empty Cart" /></td>
              <td width="372"></td>
              <td width="196"></td>
              <td width="426"><input id="goldButton" type="submit" value="Checkout" onclick="window.location='checkout.php'" /></td>
            </tr>
        </table>
        
    </div><!-----Main body ends here---->
  </div><!--container end-->
  
  <!----JS starts here----->
  
</body>
</html>