<?php
if(basename($_SERVER["HTTP_REFERER"])!='checkout.php')
  header("Location: index.php");
  
?>
<?php require("_includes/header.php"); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="main.css" />
    <title>KSU Book Store</title>
</head>

<body>
  <div id="container">
    
    
   
    <div id="mainBody">
    	<!-----Main body starts here---->
        
        
        <article>
       	   <div id="invoicenumber" style="text-align:center; width:90%;">
		   <p>Thank you for the purchase, here's the summary of items you purchased.<br/> An invoice email have been sent to your email address<br/>If you have purchased an eBook, an eBook link have been sent to your email too.</p>
		   </div>
			<div id="itemInfo" style="margin-top:5%;"><h2>Summary</h2>
			</div>
			<div id="thanksItemContainer" style="width:90%;">
			<div class="itemDetailsContainer" id="itemDetailLabel">
				<div class="itemDetails">
				<p class="number-itemInfo">ItemNumber</p>
				<p class="title-itemInfo">Description</p>
				<p class="qty-itemInfo">Quantity</p>
				<p class="price-itemInfo">Price</p>
		
			</div>
			</div>
			<?php 
			$i=0;
			foreach($_SESSION["cart"] as $eachCartItem){ $book = new Book(); $book = $book->find_by_id($eachCartItem->id);$i++?>
			<div class="itemDetailsContainer">
				<div class="itemDetails">
				<p class="number-itemInfo"><?php echo $i;?></p>
				<p class="title-itemInfo"><?php echo $book->title;?><br/>by <?php echo $book->author;?><br/>ISBN:<?php echo $book->isbn; ?></p>
				<p class="qty-itemInfo"><?php echo $eachCartItem->quantity;?></p>
				<p class="price-itemInfo"><?php echo $eachCartItem->price;?><br/><?php echo $eachCartItem->typeText; ?></p>
		
			</div>
			</div>
			<?}?>
				</div>
			<div id="finalPrice">
			
			 <?php 
			 $i=0;
			 foreach($_SESSION["cart"] as $eachCartItem){
					  
					  if($eachCartItem->type!=4){
						  //$qty=$eachCartItem->quantity;
						  //$i+=$qty;
						  $i=1;
					  }
					  
				  }
				  $shipping= 14.99*$i;
				  $tax = ($_SESSION["totalPrice"]/100)*7.5; $tax=number_format($tax, 2, '.', '');
				$price = $_SESSION["totalPrice"]; $price=number_format($price, 2, '.', '');
				$total=$shipping+$tax+$price;
				?>
		<p id="total">Total</p><p id="totalvalue"><?php echo $total;?></p>
		
		<?php
		foreach($_SESSION as $key => $val)
		{
			if ($key != 'username')
			{
			  unset($_SESSION[$key]);
			}
		}
		?>
		</div>
		
		
		
		</article>
		
		
        
        
        
        
        
        
    </div><!-----Main body ends here---->
  </div><!--container end-->
  
  <!----JS starts here----->
  <script>
  	$("#subTotal").html("0.00");
	$("#totalItem").html("0");
	console.log()
  </script>
		</body> 
        </html>