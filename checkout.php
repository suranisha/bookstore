<?php
ob_start();
require("_includes/header.php"); 
require("_includes/creditcard.php"); 
require("_includes/billing_shipping.php"); 
require("_includes/invoice.php"); 
require("_includes/login.php"); 
if($_SESSION["totalPrice"]<=0){
		
		$errorArray[]="Please add atleast one item in cart";
	}		
if(empty($errorArray)){	
//get billing and shipping first, then ask for payment. store billing and shipping in session	
		
		if(isset($_POST['shipping_billing'])){
	$Baddress=input_validate($_POST['billing-address']);
	$Bcity=input_validate($_POST['billing-city']);
	$Bstate=input_validate($_POST['billing-state']);
	$Bzip=input_validate($_POST['billing-zip-code']);
	$Bphone=input_validate($_POST['billing-phone']);
	$Bemail=input_validate($_POST['billing-email']);
	$Bname=input_validate($_POST['billing-fname']);
	$Blname=input_validate($_POST['billing-lname']);
	
	$Saddress=input_validate($_POST['shipping-address']);
	$Scity=input_validate($_POST['shipping-city']);
	$Sstate=input_validate($_POST['shipping-state']);
	$Szip=input_validate($_POST['shipping-zip-code']);
	$Sphone=input_validate($_POST['shipping-phone']);
	$Semail=input_validate($_POST['shipping-email']);
	$Sname=input_validate($_POST['shipping-fname']);
	$Slname=input_validate($_POST['shipping-lname']);
	$validateAddressB=new billingCheck($Baddress,$Bcity,$Bstate,$Bzip,$Bphone,$Bemail,$Bname,$Blname);
	$validateAddressS=new billingCheck($Saddress,$Scity,$Sstate,$Szip,$Sphone,$Semail,$Sname,$Slname);
	$checkEmptyB=$validateAddressB->emptyCheck();
	$checkEmptyS=$validateAddressS->emptyCheck();
	if((!(empty($checkEmptyB))) || (!(empty($checkEmptyS)))){
		echo "<div id='errorA'>ERROR:";
			foreach($checkEmptyB as $errors){
		
		echo "<p>Error in Billing Address:".$errors."</p>";
	}
	foreach($checkEmptyS as $errors){
		
		echo "<p>Error in Shipping Address:".$errors."</p>";
	}
	echo "</div>";
		
	}else{
		$_SESSION["Bfname"]=ucfirst(strtolower($Bname));
		$_SESSION["Blname"]=ucfirst(strtolower($Blname));
		$_SESSION["Baddress"]=$Baddress;
		$_SESSION["Bcity"]=ucfirst(strtolower($Bcity));
		$_SESSION["Bstate"]=$Bstate;
		$_SESSION["Bzip"]=$Bzip;
		$_SESSION["Bphone"]=$Bphone;
		$_SESSION["Bemail"]=$Bemail;
		
		$_SESSION["Sfname"]=ucfirst(strtolower($Sname));
		$_SESSION["Slname"]=ucfirst(strtolower($Slname));
		$_SESSION["Saddress"]=$Saddress;
		$_SESSION["Scity"]=ucfirst(strtolower($Scity));
		$_SESSION["Sstate"]=$Sstate;
		$_SESSION["Szip"]=$Szip;
		$_SESSION["Sphone"]=$Sphone;
		$_SESSION["Semail"]=$Semail;

	}
	
	
	
	
	
	}


//get billing and shipping first, then ask for payment. store billing and shipping in session	
			//Purchase CreditCard
		if(isset($_POST['purchasecc'])){
	$ccnumber=input_validate($_POST['ccnumber']);
	$ccname=input_validate($_POST['ccname']);
	$cvv=input_validate($_POST['cvv']);
	$expdatey=input_validate($_POST['exp-date-y']);
	$expdatem=input_validate($_POST['exp-date-month']);
	$validateAll=new creditcardCheck($ccnumber,$ccname,$cvv,$expdatey,$expdatem);
	
	$checkEmpty=$validateAll->emptyCheck();
	if(!(checkItemLastTimeInStock())){
		$checkEmpty[]="One or more item out of stock";
	}
	
	if(!(empty($checkEmpty))){
		echo "<div id='errorA'>ERROR:";
			foreach($checkEmpty as $errors){
		
		echo "<p>".$errors."</p>";
	}
	echo "</div>";
		
	}else{
		
		$invoice= new invoice();
		$invoice->invoiceG();
		foreach($_SESSION["cart"] as $eachCartItem){
				  if($eachCartItem->type==4){				  
						  for($i=0;$i<$eachCartItem->quantity;$i++){
						  $url=NULL;
						 $invoice->eBookEmail($url);
						  }
						}else{
					 $prices = new Price();
				$prices=$prices->find_by_id_type($eachCartItem->id,$eachCartItem->type);
				  $prices->quantity=($prices->quantity)-($eachCartItem->quantity);
				  $prices->type=$eachCartItem->type;
				  $prices->price=$eachCartItem->price;
				  $prices->updateDecrement();				 
					  }
				  }
		ob_end_clean();
		header("Location:summary.php");
	}}

	//Purchase PayPal
	if(isset($_POST['purchasepp'])){
			$email=input_validate($_POST['emailPP']);
			$password=input_validate($_POST['passwordPP']);
			$validateAll=new paypalCheck($email,$password);
			$checkEmpty=$validateAll->emptyCheck();
				if(!(checkItemLastTimeInStock())){
		$checkEmpty[]="One or more item out of stock";
	}
			if(!(empty($checkEmpty))){
		echo "<div id='errorA'>ERROR:";
			foreach($checkEmpty as $errors){
		
		echo "<p>".$errors."</p>";
	}
	echo "</div>";
		
	}else{
		
		$invoice= new invoice();
		$invoice->invoiceG();
		foreach($_SESSION["cart"] as $eachCartItem){
				  
					  if($eachCartItem->type==4){
						  						  
						  for($i=0;$i<$eachCartItem->quantity;$i++){
						  $url=NULL;
						 $invoice->eBookEmail($url);
					  }}else{
					 $prices = new Price();
				$prices=$prices->find_by_id_type($eachCartItem->id,$eachCartItem->type);
				  $prices->quantity=($prices->quantity)-($eachCartItem->quantity);
				  $prices->type=$eachCartItem->type;
				  $prices->price=$eachCartItem->price;
				  $prices->updateDecrement();				 
					  }
					  }
					  
				  }
		ob_end_clean();
		header("Location:summary.php");
	}
	
	//Purchase FAID
if(isset($_POST['purchaseFF'])){
	$user= new Login($_SESSION['username']);
	$aid=$user->getFaidBal();
	$balance=$aid-$_SESSION['totalPriceFa'];
	$up=$user->updateBal($balance);
	$invoice= new invoice();
		$invoice->invoiceG();
		foreach($_SESSION["cart"] as $eachCartItem){
				  
					  if($eachCartItem->type==4){
						  						  
						  for($i=0;$i<$eachCartItem->quantity;$i++){
						  $url=NULL;
						 $invoice->eBookEmail($url);
					  }}else{
					 $prices = new Price();
				$prices=$prices->find_by_id_type($eachCartItem->id,$eachCartItem->type);
				  $prices->quantity=($prices->quantity)-($eachCartItem->quantity);
				  $prices->type=$eachCartItem->type;
				  $prices->price=$eachCartItem->price;
				  $prices->updateDecrement();				 
					  }
					  }
					  
				  
		ob_end_clean();
		header("Location:summary.php");
	}
	
	


	?>
	
	
    <div id="mainBody">
    	<!-----Main body starts here---->
<script>
  function showDiv(divID)
   {
       if(divID=='ppTable'){hidID='ccTable'; hidIDD='faid';}else if(divID=='ccTable'){hidID='ppTable'; hidIDD='faid';}else{hidID='ppTable'; hidIDD='ccTable';}
	   
	   if(document.getElementById(divID).style.display=='none')
       {
           document.getElementById(divID).style.display='block';
			 document.getElementById(hidID).style.display='none';  
			 document.getElementById(hidIDD).style.display='none'; 
       }
}
function sameAddress(f) {
  if(f.billingtoo.checked == true) {
	f.shippingname.value = f.billingname.value;  
	f.shippinglname.value = f.billinglname.value;
    f.shippingaddress.value = f.billingaddress.value;
	f.shippingcity.value=f.billingcity.value;
	f.shippingstate.value=f.billingstate.value;
	f.shippingzipcode.value=f.billingzipcode.value;
	f.shippingphone.value=f.billingphone.value;
	f.shippingemail.value=f.billingemail.value;
   }else{
	   f.shippingname.value = "";  
	f.shippinglname.value = "";
    f.shippingaddress.value= "";
	f.shippingcity.value = "";
	f.shippingstate.value= "Select State";
	f.shippingzipcode.value= "";
	f.shippingphone.value= "";
	f.shippingemail.value= "";
	}
}
</script>     
        <article>

	<?php if(((isset($_POST['shipping_billing'])) && empty($checkEmptyS) && empty($checkEmptyB)) || (isset($_POST['purchasepp'])) || (isset($_POST['purchasecc']))){
		
		?>
           <h2>	Payment Information - Please select method of payment</h2>
		<form>
		
		<input id="blueButton" type="button" name="creditcard" href="#" value="Credit Card" onclick='showDiv("ccTable")'/>
		<input id="blueButton" name="paypal" type="button" value="PayPal" onclick='showDiv("ppTable")'/>
        <?php if($_SESSION['username']){ $log = new Login($_SESSION['username']);?><input id="goldButton" type="button" onclick='showDiv("faid")' value="Financial Aid"/></td><?php }?>
		</form>
	
        <form action="" method="POST">
		<div id="ccTable"  style="display:none;">
        <table id="tableForm">
            <tr>
              <td width="472">Credit Card Number<br>
			  <input class="formMed" type="text" name="ccnumber" value="<?php echo returnValueCheck($ccnumber);?>"/>
			  </td>
              <td width="274">Exp Date<br>
			  <select type="options" name="exp-date-month">
			   <option <?php if(returnValueCheck($expdatem)==NULL) echo "selected";?>>Select Month</option>
			  <option <?php if(returnValueCheck($expdatem)=="01") echo "selected";?>>01</option>
			  <option <?php if(returnValueCheck($expdatem)=="02") echo "selected";?>>02</option>
			  <option <?php if(returnValueCheck($expdatem)=="03") echo "selected";?>>03</option>
			  <option <?php if(returnValueCheck($expdatem)=="04") echo "selected";?>>04</option>
			  <option <?php if(returnValueCheck($expdatem)=="05") echo "selected";?>>05</option>
			  <option <?php if(returnValueCheck($expdatem)=="06") echo "selected";?>>06</option>
			  <option <?php if(returnValueCheck($expdatem)=="07") echo "selected";?>>07</option>
			  <option <?php if(returnValueCheck($expdatem)=="08") echo "selected";?>>08</option>
			  <option <?php if(returnValueCheck($expdatem)=="09") echo "selected";?>>09</option>
			  <option <?php if(returnValueCheck($expdatem)=="10") echo "selected";?>>10</option>
			  <option <?php if(returnValueCheck($expdatem)=="11") echo "selected";?>>11</option>
			  <option <?php if(returnValueCheck($expdatem)=="12") echo "selected";?>>12</option>
			  </select>
			  <input class="formSmall" type="text" name="exp-date-y" placeholder="YYYY" value="<?php echo returnValueCheck($expdatey);?>"/>
              </td>
              <td width="284">CVV<br>
			  <input class="formSmall" type="text" name="cvv" value="<?php echo returnValueCheck($cvv);?>"/>
              </td>
            </tr>
            <tr>
              <td>Name on Card<br>
			  <input class="formMed" type="text" name="ccname" value="<?php echo returnValueCheck($ccname);?>"/>
              </td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td colspan="3">
			  

			  
			  <input id="blueButton" name="purchasecc" type="submit" value="Buy Now"/>
            </tr>
            <tr>
              <td colspan="3"></td>
            </tr>
        </table>
		</div>
        </form>
		
		 <form action="" method="POST">
		<div id="ppTable"  style="display:none;">
        <table id="tableForm">
            <tr>
              <td width="472">PayPal Email<br>
			  <input class="formMed" type="email" name="emailPP" />
			  </td>
            </tr>
            <tr>
              <td>Password<br>
			  <input class="formMed" type="password" name="passwordPP" />
              </td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td colspan="3">
			  

			  
			  <input id="blueButton" name="purchasepp" type="submit" value="Buy Now"/>
            </tr>
            <tr>
              <td colspan="3"></td>
            </tr>
        </table>
		</div>
        </form>
		
		 <form action="" method="POST">
		<div id="faid"  style="display:none;">
        <table id="tableForm">
            <tr>
              <td width="472">Username<br>
			  <input class="formMed" type="text" name="username" value="<?php echo $_SESSION['username'];?>" readonly />
			  </td>
            </tr>
            <tr>
              <td>Financial Aid Balance<br>
			  <input class="formMed" type="text" name="faidbalance" value="<?php echo $log->getFaidBal();?>" readonly />
              </td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td colspan="3">
			  

			  
			  <?php if($_SESSION['totalPriceFa']<$log->getFaidBal()){?><input id="blueButton" name="purchaseFF" type="submit" value="Buy Now"/><?php }?>
            </tr>
            <tr>
              <td colspan="3"></td>
            </tr>
        </table>
		</div>
        </form>
<?php }else{

	

	?>
	  
	  <form action="" method="POST" autocomplete="on">
	
        <h2>Billing Address</h2>
        
              <table id="tableForm">
			  <tr>
			   <td width="20%">First Name<br>
             <input class="formMed" type="text" id="billingname" name="billing-fname" value="<?php echo returnValueCheck($Bname);?>"/>
              </td>
			  <td width="20%">Last Name<br>
             <input class="formMed" type="text" id="billinglname" name="billing-lname" value="<?php echo returnValueCheck($Blname);?>"/>
              </td>
			  </tr>
            <tr>
              <td colspan="3">Street Address<br/>
                <input class="formLrg" type="text" id="billingaddress" name="billing-address" value="<?php echo returnValueCheck($Baddress);?>"/>
              </td>
              <td width="29%">Final Total</td>
            </tr>
            <tr>
              <td width="20%">City<br>
             <input class="formMed" type="text" id="billingcity" name="billing-city" value="<?php echo returnValueCheck($Bcity);?>"/>
              </td>
              <td width="23%">State<br>
    <select type="options" name="billing-state" id="billingstate">
	<option <?php if(returnValueCheck($Bstate)==NULL) echo "selected";?>>Select State</option>
	<option value="AL" <?php if(returnValueCheck($Bstate)=="AL") echo "selected";?>>Alabama</option>
	<option value="AK" <?php if(returnValueCheck($Bstate)=="AK") echo "selected";?>>Alaska</option>
	<option value="AZ" <?php if(returnValueCheck($Bstate)=="AZ") echo "selected";?>>Arizona</option>
	<option value="AR" <?php if(returnValueCheck($Bstate)=="AR") echo "selected";?>>Arkansas</option>
	<option value="CA" <?php if(returnValueCheck($Bstate)=="CA") echo "selected";?>>California</option>
	<option value="CO" <?php if(returnValueCheck($Bstate)=="CO") echo "selected";?>>Colorado</option>
	<option value="CT" <?php if(returnValueCheck($Bstate)=="CT") echo "selected";?>>Connecticut</option>
	<option value="DE" <?php if(returnValueCheck($Bstate)=="DE") echo "selected";?>>Delaware</option>
	<option value="DC" <?php if(returnValueCheck($Bstate)=="DC") echo "selected";?>>District Of Columbia</option>
	<option value="FL" <?php if(returnValueCheck($Bstate)=="FL") echo "selected";?>>Florida</option>
	<option value="GA" <?php if(returnValueCheck($Bstate)=="GA") echo "selected";?>>Georgia</option>
	<option value="HI" <?php if(returnValueCheck($Bstate)=="HI") echo "selected";?>>Hawaii</option>
	<option value="ID" <?php if(returnValueCheck($Bstate)=="ID") echo "selected";?>>Idaho</option>
	<option value="IL" <?php if(returnValueCheck($Bstate)=="IL") echo "selected";?>>Illinois</option>
	<option value="IN" <?php if(returnValueCheck($Bstate)=="IN") echo "selected";?>>Indiana</option>
	<option value="IA" <?php if(returnValueCheck($Bstate)=="IA") echo "selected";?>>Iowa</option>
	<option value="KS" <?php if(returnValueCheck($Bstate)=="KS") echo "selected";?>>Kansas</option>
	<option value="KY" <?php if(returnValueCheck($Bstate)=="KY") echo "selected";?>>Kentucky</option>
	<option value="LA" <?php if(returnValueCheck($Bstate)=="LA") echo "selected";?>>Louisiana</option>
	<option value="ME" <?php if(returnValueCheck($Bstate)=="ME") echo "selected";?>>Maine</option>
	<option value="MD" <?php if(returnValueCheck($Bstate)=="MD") echo "selected";?>>Maryland</option>
	<option value="MA" <?php if(returnValueCheck($Bstate)=="MA") echo "selected";?>>Massachusetts</option>
	<option value="MI" <?php if(returnValueCheck($Bstate)=="MI") echo "selected";?>>Michigan</option>
	<option value="MN" <?php if(returnValueCheck($Bstate)=="MN") echo "selected";?>>Minnesota</option>
	<option value="MS" <?php if(returnValueCheck($Bstate)=="MS") echo "selected";?>>Mississippi</option>
	<option value="MO" <?php if(returnValueCheck($Bstate)=="MO") echo "selected";?>>Missouri</option>
	<option value="MT" <?php if(returnValueCheck($Bstate)=="MT") echo "selected";?>>Montana</option>
	<option value="NE" <?php if(returnValueCheck($Bstate)=="NE") echo "selected";?>>Nebraska</option>
	<option value="NV" <?php if(returnValueCheck($Bstate)=="NV") echo "selected";?>>Nevada</option>
	<option value="NH" <?php if(returnValueCheck($Bstate)=="NH") echo "selected";?>>New Hampshire</option>
	<option value="NJ" <?php if(returnValueCheck($Bstate)=="NJ") echo "selected";?>>New Jersey</option>
	<option value="NM" <?php if(returnValueCheck($Bstate)=="NM") echo "selected";?>>New Mexico</option>
	<option value="NY" <?php if(returnValueCheck($Bstate)=="NY") echo "selected";?>>New York</option>
	<option value="NC" <?php if(returnValueCheck($Bstate)=="NC") echo "selected";?>>North Carolina</option>
	<option value="ND" <?php if(returnValueCheck($Bstate)=="ND") echo "selected";?>>North Dakota</option>
	<option value="OH" <?php if(returnValueCheck($Bstate)=="OH") echo "selected";?>>Ohio</option>
	<option value="OK" <?php if(returnValueCheck($Bstate)=="OK") echo "selected";?>>Oklahoma</option>
	<option value="OR" <?php if(returnValueCheck($Bstate)=="OR") echo "selected";?>>Oregon</option>
	<option value="PA" <?php if(returnValueCheck($Bstate)=="PA") echo "selected";?>>Pennsylvania</option>
	<option value="RI" <?php if(returnValueCheck($Bstate)=="RI") echo "selected";?>>Rhode Island</option>
	<option value="SC" <?php if(returnValueCheck($Bstate)=="SC") echo "selected";?>>South Carolina</option>
	<option value="SD" <?php if(returnValueCheck($Bstate)=="SD") echo "selected";?>>South Dakota</option>
	<option value="TN" <?php if(returnValueCheck($Bstate)=="TN") echo "selected";?>>Tennessee</option>
	<option value="TX" <?php if(returnValueCheck($Bstate)=="TX") echo "selected";?>>Texas</option>
	<option value="UT" <?php if(returnValueCheck($Bstate)=="UT") echo "selected";?>>Utah</option>
	<option value="VT" <?php if(returnValueCheck($Bstate)=="VT") echo "selected";?>>Vermont</option>
	<option value="VA" <?php if(returnValueCheck($Bstate)=="VA") echo "selected";?>>Virginia</option>
	<option value="WA" <?php if(returnValueCheck($Bstate)=="WA") echo "selected";?>>Washington</option>
	<option value="WV" <?php if(returnValueCheck($Bstate)=="WV") echo "selected";?>>West Virginia</option>
	<option value="WI" <?php if(returnValueCheck($Bstate)=="Wi") echo "selected";?>>Wisconsin</option>
	<option value="WY" <?php if(returnValueCheck($Bstate)=="WY") echo "selected";?>>Wyoming</option>
</select>	
              </td>
              <td width="28%">Zip Code<br>
              <input class="formMed" type="text" id="billingzipcode" placeholder="XXXXX" name="billing-zip-code" maxlength=5 value="<?php echo returnValueCheck($Bzip);?>"/>
              </td>
              <td width="29%" rowspan="2" bgcolor="#f2f2f2"><table id="tableTotal">
                  <tr>
                    <td>Subtotal:</td>
                    <td>$<? $price = $_SESSION["totalPrice"]; $price=number_format($price, 2, '.', ''); echo $price; ?></td>
                  </tr>
                  <tr>
				  <?php
				  $qty=0;
				  foreach($_SESSION["cart"] as $eachCartItem){
					  
					  if($eachCartItem->type!=4){
						  $qty=1;
					  }
					  
				  }?>
                    <td>Shipping:</td>
                    <td>$<?php $shipping=14.99*$qty; echo $shipping?></td>
                  </tr>
                  <tr>
                    <td>Tax:</td>
                    <td>$<? $tax = ($_SESSION["totalPrice"]/100)*7.5; $tax=number_format($tax, 2, '.', ''); echo $tax; ?></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>Total:</td>
                    <td>$ <? $total = $price+$tax+$shipping; echo $total;
					$_SESSION['totalPriceFa']=$total;
					?></td>
                  </tr>
              </table></td>
            </tr>
            <tr>
              <td>Phone Number<br>
              <input class="formMed" type="text" id="billingphone" placeholder="4705783801" name="billing-phone" value="<?php echo returnValueCheck($Bphone);?>"/>
              </td>
              <td colspan="2">Email<br>
                <input class="formLrg" type="text" id="billingemail" placeholder="jlartigu@kennesaw.edu" name="billing-email" value="<?php echo returnValueCheck($Bemail);?>"/>
              </td>
            </tr>
            <tr>
              <td colspan="3"></td>
              <td><input id="goldButton" type="submit" name="shipping_billing" value="Proceed to Payment Method"/></td>
            </tr>
        </table>
		
        <h2>Shipping Address</h2>
       <table id="tableForm">
			<tr>
			<input type="checkbox" name="billingtoo" id="checkbox" onclick="sameAddress(this.form)">
			Check this box if shipping is same as billing address
			
			</tr>
			<tr>
			   <td width="20%">First Name<br>
             <input class="formMed" type="text" id="shippingname" name="shipping-fname" value="<?php echo returnValueCheck($Sname);?>"/>
              </td>
			  <td width="20%">Last Name<br>
             <input class="formMed" type="text" id="shippinglname" name="shipping-lname" value="<?php echo returnValueCheck($Slname);?>"/>
              </td>
			  </tr>
            <tr>
				
              <td colspan="3">Street Address<br>
                <input class="formLrg" type="text" id="shippingaddress" name="shipping-address" value="<?php echo returnValueCheck($Saddress);?>"/>
              </td>
              <td width="29%" rowspan="3"></td>
            </tr>
            <tr>
              <td width="20%">City<br>
              <input class="formMed" type="text" id="shippingcity" name="shipping-city" value="<?php echo returnValueCheck($Scity);?>"/>
              </td>
              <td width="23%">State<br>
			  <select type="options" name="shipping-state" id="shippingstate">
	<option <?php if(returnValueCheck($Sstate)==NULL) echo "selected";?>>Select State</option>
	<option value="AL" <?php if(returnValueCheck($Sstate)=="AL") echo "selected";?>>Alabama</option>
	<option value="AK" <?php if(returnValueCheck($Sstate)=="AK") echo "selected";?>>Alaska</option>
	<option value="AZ" <?php if(returnValueCheck($Sstate)=="AZ") echo "selected";?>>Arizona</option>
	<option value="AR" <?php if(returnValueCheck($Sstate)=="AR") echo "selected";?>>Arkansas</option>
	<option value="CA" <?php if(returnValueCheck($Sstate)=="CA") echo "selected";?>>California</option>
	<option value="CO" <?php if(returnValueCheck($Sstate)=="CO") echo "selected";?>>Colorado</option>
	<option value="CT" <?php if(returnValueCheck($Sstate)=="CT") echo "selected";?>>Connecticut</option>
	<option value="DE" <?php if(returnValueCheck($Sstate)=="DE") echo "selected";?>>Delaware</option>
	<option value="DC" <?php if(returnValueCheck($Sstate)=="DC") echo "selected";?>>District Of Columbia</option>
	<option value="FL" <?php if(returnValueCheck($Sstate)=="FL") echo "selected";?>>Florida</option>
	<option value="GA" <?php if(returnValueCheck($Sstate)=="GA") echo "selected";?>>Georgia</option>
	<option value="HI" <?php if(returnValueCheck($Sstate)=="HI") echo "selected";?>>Hawaii</option>
	<option value="ID" <?php if(returnValueCheck($Sstate)=="ID") echo "selected";?>>Idaho</option>
	<option value="IL" <?php if(returnValueCheck($Sstate)=="IL") echo "selected";?>>Illinois</option>
	<option value="IN" <?php if(returnValueCheck($Sstate)=="IN") echo "selected";?>>Indiana</option>
	<option value="IA" <?php if(returnValueCheck($Sstate)=="IA") echo "selected";?>>Iowa</option>
	<option value="KS" <?php if(returnValueCheck($Sstate)=="KS") echo "selected";?>>Kansas</option>
	<option value="KY" <?php if(returnValueCheck($Sstate)=="KY") echo "selected";?>>Kentucky</option>
	<option value="LA" <?php if(returnValueCheck($Sstate)=="LA") echo "selected";?>>Louisiana</option>
	<option value="ME" <?php if(returnValueCheck($Sstate)=="ME") echo "selected";?>>Maine</option>
	<option value="MD" <?php if(returnValueCheck($Sstate)=="MD") echo "selected";?>>Maryland</option>
	<option value="MA" <?php if(returnValueCheck($Sstate)=="MA") echo "selected";?>>Massachusetts</option>
	<option value="MI" <?php if(returnValueCheck($Sstate)=="MI") echo "selected";?>>Michigan</option>
	<option value="MN" <?php if(returnValueCheck($Sstate)=="MN") echo "selected";?>>Minnesota</option>
	<option value="MS" <?php if(returnValueCheck($Sstate)=="MS") echo "selected";?>>Mississippi</option>
	<option value="MO" <?php if(returnValueCheck($Sstate)=="MO") echo "selected";?>>Missouri</option>
	<option value="MT" <?php if(returnValueCheck($Sstate)=="MT") echo "selected";?>>Montana</option>
	<option value="NE" <?php if(returnValueCheck($Sstate)=="NE") echo "selected";?>>Nebraska</option>
	<option value="NV" <?php if(returnValueCheck($Sstate)=="NV") echo "selected";?>>Nevada</option>
	<option value="NH" <?php if(returnValueCheck($Sstate)=="NH") echo "selected";?>>New Hampshire</option>
	<option value="NJ" <?php if(returnValueCheck($Sstate)=="NJ") echo "selected";?>>New Jersey</option>
	<option value="NM" <?php if(returnValueCheck($Sstate)=="NM") echo "selected";?>>New Mexico</option>
	<option value="NY" <?php if(returnValueCheck($Sstate)=="NY") echo "selected";?>>New York</option>
	<option value="NC" <?php if(returnValueCheck($Sstate)=="NC") echo "selected";?>>North Carolina</option>
	<option value="ND" <?php if(returnValueCheck($Sstate)=="ND") echo "selected";?>>North Dakota</option>
	<option value="OH" <?php if(returnValueCheck($Sstate)=="OH") echo "selected";?>>Ohio</option>
	<option value="OK" <?php if(returnValueCheck($Sstate)=="OK") echo "selected";?>>Oklahoma</option>
	<option value="OR" <?php if(returnValueCheck($Sstate)=="OR") echo "selected";?>>Oregon</option>
	<option value="PA" <?php if(returnValueCheck($Sstate)=="PA") echo "selected";?>>Pennsylvania</option>
	<option value="RI" <?php if(returnValueCheck($Sstate)=="RI") echo "selected";?>>Rhode Island</option>
	<option value="SC" <?php if(returnValueCheck($Sstate)=="SC") echo "selected";?>>South Carolina</option>
	<option value="SD" <?php if(returnValueCheck($Sstate)=="SD") echo "selected";?>>South Dakota</option>
	<option value="TN" <?php if(returnValueCheck($Sstate)=="TN") echo "selected";?>>Tennessee</option>
	<option value="TX" <?php if(returnValueCheck($Sstate)=="TX") echo "selected";?>>Texas</option>
	<option value="UT" <?php if(returnValueCheck($Sstate)=="UT") echo "selected";?>>Utah</option>
	<option value="VT" <?php if(returnValueCheck($Sstate)=="VT") echo "selected";?>>Vermont</option>
	<option value="VA" <?php if(returnValueCheck($Sstate)=="VA") echo "selected";?>>Virginia</option>
	<option value="WA" <?php if(returnValueCheck($Sstate)=="WA") echo "selected";?>>Washington</option>
	<option value="WV" <?php if(returnValueCheck($Sstate)=="WV") echo "selected";?>>West Virginia</option>
	<option value="WI" <?php if(returnValueCheck($Sstate)=="Wi") echo "selected";?>>Wisconsin</option>
	<option value="WY" <?php if(returnValueCheck($Sstate)=="WY") echo "selected";?>>Wyoming</option>
</select>				
			
              </td>
              <td width="28%">Zip Code<br>
              <input class="formMed" type="text" id="shippingzipcode" name="shipping-zip-code" maxlength=5 value="<?php echo returnValueCheck($Szip);?>"/>
              </td>
            </tr>
            <tr>
              <td>Phone Number<br>
              <input class="formMed" type="text" id="shippingphone" name="shipping-phone" value="<?php echo returnValueCheck($Sphone);?>"/>
              </td>
              <td colspan="2">Email<br>
              <input class="formLrg" type="text" id="shippingemail" name="shipping-email" value="<?php echo returnValueCheck($Semail);?>"/>
              </td>
            </tr>
        </table>
	   </form>
       	<?php
}
}else{
		echo "<div id='errorA'>ERROR:";
			foreach($errorArray as $errors){
		
		echo "<p>".$errors."</p>";
	}
	echo "</div>";
}
		
	
	?>
        </article>
        
    </div><!-----Main body ends here---->

  </div><!--container end-->
  
  <!----JS starts here----->
  
</body>
</html>