<?php require("_includes/header.php");//DO NOT REMOVE 
	if(!isset($_GET["id"])) redirect_to("index.php");
	$Book = new Book(); 
	$book = $Book->find_by_id($_GET["id"]); 
	if(empty($book)) redirect_to("index.php");
	$Price = new Price(); 
	$price = $Price->find_by_id($_GET["id"]); 
	if(empty($price)) redirect_to("index.php");
?>

<div id="mainBody">
    	<!-----Main body starts here---->
       <div id="book_details">
		<div id="book_details_row1">
			<div id="coverimage">
            	<? if(file_exists("images/bookCover/{$book->isbn}.jpg")){ ?>
                    <img src="images/bookCover/<? echo $book->isbn; ?>.jpg" width="200" height="249" alt=""/>
                <? }else{ ?>
                    <img src="images/bookCover/noImage.jpg" width="200" height="249" alt=""/>
                <? } ?>
                <p id="isbn">ISBN: <? echo $book->isbn; ?><br/><? 
				if($book->required){
					echo "<span class=\"required\">Required</span>";
				}else{
					echo "<span class=\"recommended\">Recommended</span>";
				} ?></p>
			</div>
            <div id="productdetail">
                <p id="title">
                <?php echo $book->title; ?>
                </p>
                <p id="author">
                <?php echo $book->author; ?>
                </p>
                
                <p id="description">
                <b>Description:</b><br/>
                <?php echo $book->description; ?>
                </p>
            </div>
		</div><!--book-details-row1-->	
        
        <div id="book_details_row2">
            <? foreach($price as $eachPrices){ 
				if($eachPrices->type==4 && $eachPrices->quantity==-1){
					continue;
				}
			?>
                  <div class="booktype">
                    <div class="booktype_col1">
                    <h2><? echo $eachPrices->typeText ?></h2>
                    
                    <!--if its not eBook-->
                    <? if($eachPrices->type!=4){ ?>
                   <h6><p class="stock"><? if($eachPrices->quantity==0){echo "<span style='color:#F00; font-size:11px;'> Out Of Stock </span>";}else{ echo "Stock: ".$eachPrices->quantity;} ?></p></h6>
                   <? } ?>
                   
                    </div>
                    <div class="price"><h3>$<? echo $eachPrices->price; ?></h3><h4>Price</h4></div>
                      <div class="submit-form-qty">
                          <? if($eachPrices->quantity!=0){ ?>
                          <div class="book-quantity">
                          	<!--<label for="book-quantity-no">Quantity</label>-->
                            <input type="number" hidden value="1" id="book-quantity-no" name="qty" min=0 required/>
                          </div>
                          <a href="#" data-book="<? echo $eachPrices->id; ?>"  data-type="<? echo $eachPrices->type; ?>"  class="button-cart animate">Add to Cart </a>
                          <? } ?>
                      </div>
                  	</div>
            <? } ?>
		</div>	   
    </div><!-----Main body ends here---->
  </div><!--container end-->
  
  <!----JS starts here----->

</body>
</html>