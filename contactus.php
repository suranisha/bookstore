<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="main.css" />
    <title>KSU Book Store</title>
</head>

<body>
  <div id="container">
    <?php require("_includes/header.php"); ?>
    
    <div id="mainBody">
    	<!-----Main body starts here---->
        
        
        <article>
       <h2>Contact Us</h2>
       &nbsp;
       <h3>Bookstore Contact Information</h3>
       <p>&nbsp;</p>
       <p>Phone: 470-578-2665</p>
       <p>Email: <a class="MB" href="mailto:ksubookstore@kennesaw.edu">ksubookstore@kennesaw.edu</a></p>
       <p>&nbsp;</p>
       <p>&nbsp; </p>
       <h3>Kennesaw State University Contact Information</h3>
       <p>&nbsp;</p>
       <p>Phone: 470-578-6000</p>
       <p>Fax: 470-578-6001</p>
       <p>Email: <a class="MB" href="mailto:contact@kennesaw.edu">contact@kennesaw.edu</a></p>
       <p>Website: <a class="MB" href="http://www.kennesaw.edu/">www.kennesaw.edu</a></p>
   
        </article>
        
        
        
        
        
        
        
    </div><!-----Main body ends here---->
  </div><!--container end-->
  
  <!----JS starts here----->
  
</body>
</html>