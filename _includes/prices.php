<?php
require_once("crud.php");

class Price extends CRUD{
	
	public $id;
	public $price;
	public $type;
	public $typeText;
	public $quantity;
	
	
	function __construct() {
		self::$table_name = "bookprice";
		self::$db_fields = array('id', 'price', 'type', 'quantity');
	}
	
	public static function find_by_id($id=0) {
    	$result_array = static::find_by_sql("SELECT * FROM ".static::$table_name." WHERE id={$id}");
		return !empty($result_array) ? $result_array : false;
 	}
	
	public function find_by_id_type($book,$type){
		$result_array = static::find_by_sql("SELECT * FROM ".static::$table_name." WHERE id={$book} AND type={$type}");
		return !empty($result_array) ? array_shift($result_array) : false;
	}
	
	protected static function instantiate($record) {
	  $object = parent::instantiate($record);
	  self::typeText($object);
	  return $object;
	}

	public function typeText(&$obj) {
    	switch($obj->type){
			case 1: $obj->typeText="New"; return;
			case 2: $obj->typeText="Used"; return;
			case 3: $obj->typeText="Rent"; return;
			case 4: $obj->typeText="eBook"; return;
		}
 	}
	
}

?>