<?php
require_once("db.php");
require_once("prices.php");
require_once("crud.php");
require_once("book.php");


class UploadBook extends CRUD{
	
	function __construct($linkToFile) {
		if($linkToFile!=""){
			self::upload_csv($linkToFile);
		}else{
			die;
		}
	}
	
	public static function upload_csv($fileLocation) {
	ini_set('auto_detect_line_endings',TRUE);
	if(file_exists($fileLocation)){	
		$obj = new Book("");
		$obj->find_by_sql("TRUNCATE TABLE book");
		$obj->find_by_sql("TRUNCATE TABLE bookprice");
	
	
	  $handle = fopen($fileLocation, "r");
		if ($handle) {
			while(!feof($handle)){
			  $data = fgetcsv($handle, 0);
			  $obj = new Book("");
			  
			  if($data[1]!=""){
			  $obj->title = $data[1]; } else continue;
			  $obj->description = $data[17];
			  $obj->author = self::author($data[2]);
			  $obj->isbn = $data[0];
			  $obj->professor = $data[6];
			  $obj->courseName = $data[4];
			  $obj->crn = $data[7];
			  $obj->semester = $data[3];
			  $obj->required = ($data[8]=="Required" || $data[8]=="required") ? 1: 0;
			  $obj->create();
			  for($i=0; $i<4; $i++){
				  if($obj->id==""){break;}
				  $prices = new Price();
				  $prices->id = $obj->id;
				  $prices->quantity=$data[$i+9];
				  $prices->type=$i+1;
				  $prices->price=$data[$i+13];
				  $prices->create();
			  }
			}
			if(!unlink($fileLocation)){
				echo "Dont have permission to delete books.csv. Please delete it manually.";
			}
		}
	  }
 	}
	private function author($author=""){
		$authors = explode(";", $author);
		if(sizeof($authors)>1){
			return trim($authors[1])." ". $authors[0];
		}
		return $author;
	}
}
?>