<?php
header('Content-Type: application/json');
require_once("crud.php");
require_once("book.php");
require_once("prices.php");
session_start();
$return = array();


if(isset($_GET["addToCart"]) && isset($_GET["type"]) && isset($_GET["quantity"] )){
	if($_GET["quantity"]<0)return;
	$book = $_GET["addToCart"];
	$type = $_GET["type"];
	$books = new Book();
	$dbBook = $books->find_by_id($book);
	$prices = new Price();
	$price = $prices->find_by_id_type($book,$type);
	$price->price = number_format($price->price, 2, '.', '');
	
	if($price->quantity >= $_GET["quantity"] || $price->type==4){
		if(!isset($_SESSION["cart"])){$_SESSION["cart"] = array();}
		if(!isset($_SESSION["totalItemsInCart"])){$_SESSION["totalItemsInCart"]=0;}
		if(!isset($_SESSION["totalPrice"])){$_SESSION["totalPrice"]=0;}
		
		foreach($_SESSION['cart'] as $c){
			if($c->id==$price->id && $c->type==$price->type){
				if($_GET["type"]==4 && $price->quantity!=0){
					$c->quantity+=$_GET['quantity'];
					$_SESSION["totalItemsInCart"]+=$_GET['quantity'];
					$_SESSION["totalPrice"] += ($_GET["quantity"] * $price->price);
				}else if(($c->quantity+$_GET['quantity'])<=$price->quantity){
					$c->quantity+=$_GET['quantity'];
					$_SESSION["totalItemsInCart"]+=$_GET['quantity'];
					$_SESSION["totalPrice"] += ($_GET["quantity"] * $price->price);
				}
				$return["totalItemsInCart"] = $_SESSION["totalItemsInCart"];
				$return["totalPrice"] = $_SESSION["totalPrice"];
				echo json_encode($return);
				return;
			}
		}
		//new item
		$price->quantity = $_GET["quantity"];
		$_SESSION["totalItemsInCart"]+=$_GET['quantity'];
		$_SESSION["totalPrice"] += ($price->quantity * $price->price);
		$_SESSION["cart"][]=$price;
		
		$return["totalItemsInCart"] = $_SESSION["totalItemsInCart"];
		$return["totalPrice"] = $_SESSION["totalPrice"];
		echo json_encode($return);
		return;
	}
}else if(isset($_GET["update"])){
	if(isset($_GET["emptyCart"])){
		session_unset();
		$_SESSION["totalPrice"]="0.00";
		$_SESSION["totalItemsInCart"] = 0;
		$return["empty"]="done";
		echo json_encode($return);
		return;
	}//empty
	
	else if(isset($_GET["remove"])){
		foreach($_SESSION["cart"] as $key => $eachBook){
			if($eachBook->id == $_GET["remove"] && $eachBook->type == $_GET["type"]){
				$_SESSION["totalItemsInCart"]-=$eachBook->quantity;
				$_SESSION["totalPrice"]-=($eachBook->price*$eachBook->quantity);
				unset($_SESSION["cart"][$key]);	
				
				$return["removed"]=$_GET["remove"];
				$return["totalPrice"]=$_SESSION["totalPrice"];
				$return["totalItemsInCart"] = $_SESSION["totalItemsInCart"];
				echo json_encode($return);
				return;
			}
		}
		return;
	}//remove
}


?>