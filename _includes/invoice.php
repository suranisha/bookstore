<?php
require_once '../upload/dompdf-master/autoload.inc.php';
require '../upload/PHPMailer-master/PHPMailerAutoload.php';
use Dompdf\Dompdf;
class invoice{
public function invoiceG(){
$dompdf = new Dompdf();
$mail = new PHPMailer;
$dompdf->set_option('isHtml5ParserEnabled', true);	
ob_start();
?>

<!doctype html> 
        <html> 
        <head> 
            <link rel="stylesheet" href="/home/oenjjhrv/public_html/main.css" type="text/css" /> 
        </head> 
        <body>
<div id="container">
    <header>
    	<img class="logo" src="/home/oenjjhrv/public_html/images/mainMenu/logo.png" />
        <div class="blackBar">
        	
        </div>
         <div class="goldBar">
         	
         </div>
    </header>
    
    <div id="mainBody">
    	<!-----Main body starts here---->
        <article>
       	   <div id="invoicenumber">
		   <h3>Invoice</h3><h5>I-<?php echo date("His");?></h5><h6>Date: <?php echo date("m/d/Y");?></h6>
		   </div>
        
<div id="billingshipping">
			<div class="billingInfo">
				Billing Information
					<div class="billingDetails">
					<?php echo $_SESSION["Bfname"].' '.$_SESSION["Blname"].'<br/><br/>
					'.$_SESSION["Baddress"].'<br/>
					'.$_SESSION["Bcity"].', '.$_SESSION["Bstate"].'. '.$_SESSION["Bzip"].'<br/><br/>
					Phone:'.$_SESSION["Bphone"].'<br/>
					'.$_SESSION["Bemail"];?>
					</div>
			</div>
			<div class="shippingInfo">
				Shipping Information
					<div class="billingDetails">
					<?php echo $_SESSION["Sfname"].' '.$_SESSION["Slname"].'<br/><br/>
					'.$_SESSION["Saddress"].'<br/>
					'.$_SESSION["Scity"].', '.$_SESSION["Sstate"].'. '.$_SESSION["Szip"].'<br/><br/>
					Phone:'.$_SESSION["Sphone"].'<br/>
					'.$_SESSION["Semail"];?>
					</div>
			</div>
			</div><!--BillingShipping-->
			<div id="itemInfo"><h2>Item Purchased</h2>
			</div>
						<div class="itemDetailsContainer" id="itemDetailLabel">
				<div class="itemDetails">
				<p class="number-itemInfo">ItemNumber</p>
				<p class="title-itemInfo">Description</p>
				<p class="qty-itemInfo">Quantity</p>
				<p class="price-itemInfo">Price</p>
		
			</div>
			</div>
			<?php
			foreach($_SESSION["cart"] as $eachCartItem){ $book = new Book(); $book = $book->find_by_id($eachCartItem->id);
			?>
			<div class="itemDetailsContainer">
				<div class="itemDetails">
				<p class="number-itemInfo">1</p>
				<p class="title-itemInfo"><?php echo $book->title.'<br/>by '.$book->author.'<br/>'.$book->isbn.'</p>
				<p class="qty-itemInfo">'.$eachCartItem->quantity.'</p>
				<p class="price-itemInfo">'.$eachCartItem->price.'<br/>'.$eachCartItem->typeText.'</p>
				
		
			</div>
			</div>
			';}?>
			<div id="finalPrice">
			<?php $price = $_SESSION["totalPrice"]; $price=number_format($price, 2, '.', '');?>
		<p id="subTotal">Subtotal</p><p class="Fvalue">$<?php echo $price;?></p><br/>
						 <?php 
						 $qty=0;
						 foreach($_SESSION["cart"] as $eachCartItem){
					  
					  if($eachCartItem->type!=4){
						  $qty=1;
					  }
					  
				  }?><p id="shipping">Shipping</p><p class="Fvalue">$<?php $shipping=14.99*$qty; echo $shipping;?></p><br/>
		<?php $tax = ($_SESSION["totalPrice"]/100)*7.5; $tax=number_format($tax, 2, '.', '');?>
		<p id="tax">Tax</p><p class="Fvalue">$<?php echo $tax;?></p><br/>
		<p id="total">Total</p><p id="totalvalue">$<?php echo $price+$tax+$shipping;?></p>
		
		</div>
		
		
		
		</article>
		
		
        
        
        
        
        
        
    </div><!-----Main body ends here---->
  </div><!--container end-->
  
  <!----JS starts here----->
  
		</body> 
        </html>
<?php
$content=ob_get_clean();
$dompdf->loadHtml($content);

// Render the HTML as PDF
$dompdf->render();
$dir="/home/oenjjhrv/public_html/upload/invoices/".date("Ymd");
//check if directory exist? if not, create a directory
if(!(is_dir($dir)) && !file_exists($dir)){
	mkdir($dir);
}
$file=$dir."/invoice".date("His").".pdf";
// Output the generated PDF to Browser
file_put_contents($file, $dompdf->output()); 
$email=$_SESSION['Bemail'];
		$mail->SetFrom('noreply@bookstore.valuehosted.com', 'KSU Bookstore');
        $mail->addAddress($email, 'Invoice');
        $mail->Subject = 'KSU Bookstore Invoice';
        $mail->msgHTML("Thank you for purchase. Attached is the invoice of your purchase");
		  $mail->addAttachment($file, 'Invoice.pdf');
		  $msg="";
 if (!$mail->send()) {
            $msg .= "Mailer Error: " . $mail->ErrorInfo;
        }

}

public function eBookEmail($url){
	$eBmail = new PHPMailer;
	if($url==NULL){
		$url="http://www.test.com/book.pdf";
	}
	$email=$_SESSION['Semail'];
		$eBmail->SetFrom('noreply@bookstore.valuehosted.com', 'KSU Bookstore');
        $eBmail->addAddress($email, 'eBook Link');
        $eBmail->Subject = 'eBook Link';
        $eBmail->msgHTML("Thank you for purchase. Here is the link to view & download your eBook.".$url);
		 // $mail->addAttachment($file, 'Invoice.pdf');
		  $msg="";
 if (!$eBmail->send()) {
            $msg .= "Mailer Error: " . $eBmail->ErrorInfo;
        }
}
}


?>