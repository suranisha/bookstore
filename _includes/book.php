<?php
require_once("crud.php");

//class Search extends CRUD{
class Book extends CRUD{
	
	function __construct() {
		self::$table_name = "book";
		self::$db_fields = array('id', 'title', 'description', 'author', 'isbn', 'professor', 'crn', 'courseName', 'semester', 'required');
	}
	
	public $id;
	public $title;
	public $description;
	public $author;
	public $isbn;
	public $professor;
	public $crn;
	public $courseName;
	public $semester;
	public $required;
	
	//bookprice
	public static function find_by_sql($sql="") {
		$searchResults = parent::find_by_sql($sql);
		return $searchResults;
  	}
	
	public static function search_by_type($type=0, $keyword) {
		switch($type){
			case "1": return self::find_by_sql("SELECT * FROM ".self::$table_name." 
			WHERE 
			title LIKE '%{$keyword}%' OR
			description LIKE '%{$keyword}%' OR
			author LIKE '%{$keyword}%' OR
			isbn LIKE '%{$keyword}%' OR
			professor LIKE '%{$keyword}%' OR
			courseName LIKE '%{$keyword}%' OR
			crn LIKE '%{$keyword}%' OR
			semester LIKE '%{$keyword}%'
			");
			case "2": return self::find_by_sql("SELECT * FROM ".self::$table_name." WHERE title LIKE '%{$keyword}%'");
			case "3": return self::find_by_sql("SELECT * FROM ".self::$table_name." WHERE author LIKE '%{$keyword}%'");
			case "4": return self::find_by_sql("SELECT * FROM ".self::$table_name." WHERE description LIKE '%{$keyword}%'");
			case "5": return self::find_by_sql("SELECT * FROM ".self::$table_name." WHERE courseName LIKE '%{$keyword}%'");
			case "6": return self::find_by_sql("SELECT * FROM ".self::$table_name." WHERE crn LIKE '%{$keyword}%'");
			case "7": return self::find_by_sql("SELECT * FROM ".self::$table_name." WHERE professor LIKE '%{$keyword}%'");
			case "8": return self::find_by_sql("SELECT * FROM ".self::$table_name." WHERE isbn LIKE '%{$keyword}%'");
		}
	}

	

}

?>