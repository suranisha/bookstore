<?php
class billingCheck{
public $address;
public $city;
public $state;
public $zip;
public $phone;
public $email;
public $AerrorArray;	
public $fname;
public $lname;
	function __construct($add,$cit,$stat,$zi,$pho,$emai,$na,$lna){
		$this->address=$add;
		$this->city=$cit;
		$this->state=$stat;
		$this->zip=$zi;
		$this->phone=$pho;
		$this->email=$emai;
		$this->AerrorArray=array();
		$this->fname=$na;
		$this->lname=$lna;

	}
	public function emptyCheck(){
		if(empty($this->address) || empty($this->city) || empty($this->state) || empty($this->zip) || empty($this->phone) || empty($this->email) || empty($this->fname) || empty($this->lname)){
			
			$this->AerrorArray[]="Please fill out all the fields";
			
		}else { 
		
			if(strlen($this->zip)!=5){
			$this->AerrorArray[]="incorrect zip code";
		}else{
		$url = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.$this->zip.'&sensor=true');
		$json = json_decode($url);	
		
		$city=$json->results[0]->address_components[1]->short_name;
		$state1=$json->results[0]->address_components[2]->short_name;
		$state=$json->results[0]->address_components[3]->short_name;
			if(!(strtolower($city)==strtolower($this->city))){
				$this->AerrorArray[]="Zip Code doesn't match city";
			}
			if(!($state==$this->state) && !($state1==$this->state)){
				$this->AerrorArray[]="Zip Code doesn't match state";
			}
			
		}
		if(!(ctype_digit($this->phone))){
			
			$this->AerrorArray[]="Incorrect Phone Number";
			
		}else if(strlen($this->phone)!=10){
			
			$this->AerrorArray[]="Incorrect Phone Number";
			
		}
		if(!(ctype_alpha(str_replace(" ","",$this->fname)))){
			$this->errorArray[]="Name must contain Alphabets only";
		}
				if(!(ctype_alpha(str_replace(" ","",$this->lname)))){
			$this->errorArray[]="Name must contain Alphabets only";
		}
		if(!(filter_var($this->email,FILTER_VALIDATE_EMAIL))){
				$this->AerrorArray[]="Invalid Email";
			}
		}
		if(!(empty($this->AerrorArray))){
			return $this->AerrorArray;
			
		}else{
			return false;
		}
		
		
	}
	
	

private function checkCity($array,$city){
	
	foreach($array as $arr){
		if(strtolower($arr)==strtolower($city)){
			return true;
		}
	}
	return false;
}
}


?>