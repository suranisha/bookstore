<?php require_once("functions.php"); ?>
<?php require_once("_includes/db.php"); ?>
<?php require_once("_includes/book.php"); ?>
<?php require_once("_includes/prices.php"); ?>
<? require_once("_includes/uploadBooks.php"); new UploadBook("csv/books.csv");  ?>

<?php
session_start();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="main.css" />
    <link rel="stylesheet" type="text/css" href="appendCSS.css" />
    
    <script src="_includes/jquery.js"></script>
    <script>
    $("document").ready(function(e) {
        $("#updateQuantityButton").on("click",function(){
			var books = $(".redButton");
			var getString = "";
			for(var i=0; i<books.length; i++){
				getString+=$(books[i]).data("id") +"_"+$(books[i]).data("type") +"="+ $(books[i]).parent("td").children("p").children("input").val();
				
				if(i != books.length-1){
					getString+="&";
				}
			}
			$.ajax({
			  url: "cart.php?"+"update=true&"+getString
			}).done(function( data ) {
				window.location='cart.php';
		  	});
		});
		
		
		$(".animate").on("click",function(){
			$(this).animate({height: "14px",width:"115px"},100,function(){$(this).animate({height: "15px",width:"120px"})});
		});
    });
    </script>
    <title>KSU Book Store</title>
    
   
  </script>
    
    
</head>

<body>
  <div id="container">
  
  <header>
    <img class="logo" src="images/mainMenu/logo.png" />
    <div class="blackBar">
        <div id="cartLogin">
            <p> <?php if(isset($_SESSION['username'])){ echo $_SESSION['username']." <a href='logout.php'>Logout</a>";}else{?><a href="login.php" > Login </a><?php }?></p>
            <div>
                <a href="cart.php" ><img src="images/mainMenu/cart_icon.png" /> </a>
            </div>
            <p> SubTotal: $<span id="subTotal"><? if(isset($_SESSION["totalPrice"])){echo $_SESSION["totalPrice"];}else{ ?>0.00<? }?></span></p>
            <p> Total Item: <span id="totalItem"><? if(isset($_SESSION["totalItemsInCart"])){echo $_SESSION["totalItemsInCart"];}else{ ?>0<? }?></span></p>
        </div>
    </div>
     <div class="goldBar">
        <nav id="mainNav">
            <ul>
              <a href="index.php"><li>Search</li></a>
              <a href="about.php"><li>About Us</li></a>
              <a href="contactus.php"><li>Contact Us</li></a>
            </ul>
        </nav>
     </div>
  </header>