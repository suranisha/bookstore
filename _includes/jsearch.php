<?php require_once("prices.php"); ?>
<?php require_once("functions.php"); ?>
<?php require_once("db.php"); ?>
<?php require_once("book.php"); ?>
<?php require_once("prices.php"); ?>

<?php
	$search = new Book();
	$searchResult = $search->search_by_type($_GET["searchBy"],$_GET["text"]);
	foreach($searchResult as $result){
 ?>

<tr>
    <td width="17%">
		<? if(file_exists("../images/bookCover/{$result->isbn}.jpg")){ ?>
    		<img src="images/bookCover/<? echo $result->isbn; ?>.jpg" width="150" height="200" alt=""/>
        <? }else{ ?>
        	<img src="images/bookCover/noImage.jpg" width="150" height="200" alt=""/>
        <? } ?>    </td>
    <td  width="68%">
    	<a  class="MB" href="bookdetails.php?id=<? echo $result->id; ?>"><h2>
			<? 
			echo $result->title; 
			if($result->required==1){
				echo "<span class=\"required\">Required</span>";
			}else{
				echo "<span class=\"recommended\">Recommended</span>";
				} ?> </h2> </a>
    	<p>By: <? echo $result->author; ?></p>
    	<p>ISBN: <? echo $result->isbn; ?></p>
        <p>Course Number: <? echo $result->courseName; ?>, CRN: <? echo $result->crn; ?></p>
        <p>Professor: <? echo $result->professor; ?></p>
        <p class="description"><? echo $result->description; ?></p>
        
    </td>
    <?php /*?><td class="rightMenu">
      <select name="type" class="asl">
      	<? $prices = new Price; $prices= $prices->find_by_id($result->id);?>
      	<?php foreach($prices as $price){ ?>
        <option value="<? echo $price->type ?>"><? echo $price->typeText?></option>
        <? } ?>
      </select>
      
      <p> Qty: 
        <input name="sub" type="button" value="-" />
        <span id="qty"><input name="qty" type="text" value="0" class="qty" /></span>
        <input name="add" type="button" value="+" /> 
      </p>
      
      <p> Price: <span id="price">$<? echo number_format($prices[0]->price, 2, '.', '') ?></span> </p>
      <p style="display: none;" id="ous"><span>Out Of Stock</span></p>
      <?php foreach($prices as $price){ ?>
      <span hidden data-type = "<? echo $price->type?>" data-price = "<? echo number_format($price->price, 2, '.', '')?>" data-instock = "<? echo $price->quantity?>"></span>
      <? } ?>
      <? if($prices[0]->quantity>0) { ?> <p> In stock: <span id="instock"><? echo $prices[0]->quantity; ?></span> </p> <? }?>
      <?php */?>
      <a href="#" data-book="<? echo $result->id; ?>"  data-type="<? echo $prices[0]->type; ?>"  class="button-cart" id="button-cart">Add to Cart </a>
    </td>
   </tr>
   
   
   <? } ?>