<?php
class creditcardCheck{
	
public $ccnumber;
public $ccname;
public $cvv;
public $expdatey;
public $expdatem;
public $errorArray;

	function __construct($ccno,$ccn,$cvvthree,$expdateyear,$expdatemonth){
		$this->ccnumber=$ccno;
		$this->ccname=$ccn;
		$this->cvv=$cvvthree;
		$this->expdatey=$expdateyear;
		$this->expdatem=$expdatemonth;
		$this->errorArray=array();
		
	}
public function emptyCheck(){
		if(empty($this->ccnumber) || empty($this->ccname) || empty($this->cvv) || empty($this->expdatey) || empty($this->expdatem)){
			
			$this->errorArray[]="Please fill in all the fields";
			
		}else if($this->expdatem=="Select Month"){
			$this->errorArray[]="Please select month in exp date";
		}else if(!(ctype_digit($this->expdatey))){
			$this->errorArray[]="Invalid Year in Expiry";
		}else if(strlen($this->expdatey)!=4){
			$this->errorArray[]="Invalid Year in Expiry";
		}else if(!($this->validate_exp_date($this->expdatem,$this->expdatey))){
			$this->errorArray[]="Credit Card Expire";
		}else if($this->cvv!=777){
			$this->errorArray[]="Invalid CVV Number";
		}else if(strlen($this->ccnumber)!=16 || !(ctype_digit($this->ccnumber))){
			$this->errorArray[]="Invalid Card Number";
		}else if(!(ctype_alpha(str_replace(" ","",$this->ccname)))){
			$this->errorArray[]="Name must contain Alphabets only";
		}
		
		if(!(empty($this->errorArray))){
			return $this->errorArray;
			
		}else{
			return false;
		}
		
		
		
		
		
	}
public function validate_exp_date($expdatem,$expdatey){
	$now = date("m/Y");
	$input_time = mktime(0,0,0,$expdatem,0,$expdatey);
	if ($input_time < time()){
		return false;
	}
	return true;
}
}
function checkItemLastTimeInStock(){
		foreach($_SESSION["cart"] as $eachCartItem){
		$prices = new Price; 
		$prices= $prices->find_by_id($eachCartItem->id);
	$type=$eachCartItem->type;
	
	foreach($prices as $checkQTY){
	if($checkQTY->type!=$type && $checkQTY->quantity==0){
		return false;
	}
	}
	}
	return true;
}
class paypalCheck{
	public $email;
	public $password;
	public $errorArray;
	
	function __construct($e,$p){
		$this->email=$e;
		$this->password=$p;
		$errorArray=array();
		
	}
public function emptyCheck(){
				
			if(empty($this->email) || empty($this->password)){
			$this->errorArray[]="Please fill in all the fields";
			}else if(!(filter_var($this->email,FILTER_VALIDATE_EMAIL))){
				$this->errorArray[]="Invalid Email";
			}
			$hash = password_hash(12345678, PASSWORD_DEFAULT);
			if(!(password_verify($this->password, $hash))){
				$this->errorArray[]="Invalid Password";
			}
			if(!(empty($this->errorArray))){
			return $this->errorArray;
			
		}else{
			return false;
		}
	
}
	
	
}
?>