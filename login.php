<?php ob_start();?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="main.css" />
    <title>KSU Book Store</title>
</head>

<body>
  <div id="container">
    <?php require("_includes/header.php"); ?>
	 <?php require("_includes/login.php"); ?>
	
    
    <div id="mainBody">
    	<!-----Main body starts here---->

        
        <article>
        <h2>User Login</h2>
		<?php
		if(isset($_SESSION['username'])){
			ob_end_clean();
			header("Location: index.php");
		}
		if(isset($_POST['loginSubmit'])){
			$log = new Login($_POST['uid']);
			if($log->loginCheck($_POST['pass'])){
				$_SESSION['username']=$_POST['uid'];
				
				ob_end_clean();
				header("Location: index.php");
				
			}else{
				echo "<div id='errorA'>ERROR:<p>Invalid username or password</p></div>";
			};
			
			
			
		}
		?>
        <form action="" method="POST">
        <table id="tableForm">
            <tr>
              <td colspan="2">KSU Net ID<br>
			  <input class="formMed" type="text" id="ID" name="uid"/><br>
              </td>
            </tr>
            <tr>
              <td colspan="2">Password<br>
			  <input class="formMed" type="password" id="password" name="pass"/>
              </td>
            </tr>
            <tr>
              <td  colspan="2"><input style="width:20%; height:30px;" id="greenButton" type="submit" value="Login" name="loginSubmit"/></td>
            </tr>
            <tr>
              <td colspan="2"></td>
            </tr>
            <tr>
              <td colspan="2"></td>
            </tr>
        </table>
        </form>
        </article>
        
    </div><!-----Main body ends here---->
  </div><!--container end-->
  
  <!----JS starts here----->
  
</body>
</html>